package films.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import films.R;
import films.models.Film;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FilmAdapter extends ArrayAdapter<Film> {
    // View lookup cache
    private static class ViewHolder {
        public ImageView ivCover;
        public TextView tvTitle;
        public TextView tvYear;
        public RatingBar tvRating;
    }

    private Context context;

    public FilmAdapter(Context context, List<Film> aFilms) {
        super(context, 0, aFilms);
        this.context = context;
    }

    // Translates a particular `Film` given a position into a relevant row within an AdapterView
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Film film = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_film, parent, false);
            viewHolder.ivCover = (ImageView)convertView.findViewById(R.id.ivBookCover);
            viewHolder.tvTitle = (TextView)convertView.findViewById(R.id.tvTitle);
            viewHolder.tvYear = (TextView)convertView.findViewById(R.id.tvYear);
            viewHolder.tvRating = (RatingBar)convertView.findViewById(R.id.tvRating);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data into the template view using the data object
        viewHolder.tvTitle.setText(film.getTitle());
        viewHolder.tvYear.setText("Year: " + film.getYear());
        viewHolder.tvRating.setRating(film.getRating().floatValue());
        Picasso.with(getContext()).load(Uri.parse(film.getPoster())).error(R.drawable.ic_nocover).into(viewHolder.ivCover);
        // Custom font
        Typeface custom_font = Typeface.createFromAsset(context.getApplicationContext().getAssets(),  "fonts/montserrat.ttf");
        viewHolder.tvTitle.setTypeface(custom_font);
        viewHolder.tvYear.setTypeface(custom_font);
        return convertView;
    }
}
