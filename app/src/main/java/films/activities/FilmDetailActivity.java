package films.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import films.R;
import films.models.Film;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FilmDetailActivity extends ActionBarActivity {
    private ImageView ivFilmCover;
    private TextView tvTitle;
    private RatingBar rbRating;
    private TextView tvDirector;
    private TextView tvSummary;
    private TextView tvCast;
    private TextView tvRuntime;
    private TextView tvCategory;
    private TextView tvYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_detail);
        // Fetch views
        ivFilmCover = (ImageView) findViewById(R.id.ivFilmCover);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        rbRating = (RatingBar) findViewById(R.id.rbRating);
        tvDirector = (TextView) findViewById(R.id.tvDirector);
        tvSummary = (TextView) findViewById(R.id.tvSummary);
        tvCast = (TextView) findViewById(R.id.tvCast);
        tvRuntime = (TextView) findViewById(R.id.tvRuntime);
        tvCategory = (TextView) findViewById(R.id.tvCategory);
        tvYear = (TextView) findViewById(R.id.tvYear);
        // Use the film to populate the data into our views
        Film film = (Film) getIntent().getSerializableExtra(FilmListActivity.FILM_DETAIL_KEY);
        loadBook(film);
        loadFont();
    }

    // Populate data for the book
    private void loadBook(Film film) {
        //change activity title
        setTitle(film.getTitle());
        // Populate data
        Picasso.with(this).load(Uri.parse(film.getPoster())).error(R.drawable.ic_nocover).into(ivFilmCover);
        tvTitle.setText(film.getTitle());
        rbRating.setRating(film.getRating().floatValue());
        tvDirector.setText(film.getDirector());
        tvSummary.setText(film.getSumary());
        tvCast.setText(film.getCastAsString());
        tvRuntime.setText("Duration: " + film.getRuntime());
        tvCategory.setText("Category: " + film.getCategory());
        tvYear.setText("Year: " + film.getYear());
    }

    // Custom font
    private void loadFont() {
        Typeface custom_font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/montserrat.ttf");
        tvTitle.setTypeface(custom_font);
        tvDirector.setTypeface(custom_font);
        tvSummary.setTypeface(custom_font);
        tvCast.setTypeface(custom_font);
        tvRuntime.setTypeface(custom_font);
        tvCategory.setTypeface(custom_font);
        tvYear.setTypeface(custom_font);
    }
}
