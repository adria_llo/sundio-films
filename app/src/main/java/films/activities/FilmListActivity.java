package films.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.loopj.android.http.JsonHttpResponseHandler;
import films.R;
import films.adapters.FilmAdapter;
import films.models.Film;
import films.net.FilmClient;
import org.apache.http.Header;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class FilmListActivity extends ActionBarActivity {
    public static final String FILM_DETAIL_KEY = "film";
    private ListView lvFilms;
    private FilmAdapter filmAdapter;
    private FilmClient client;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_list);
        lvFilms = (ListView) findViewById(R.id.lvFilms);
        List<Film> aFilms = new ArrayList<>();
        // initialize the adapter
        filmAdapter = new FilmAdapter(this, aFilms);
        // attach the adapter to the ListView
        lvFilms.setAdapter(filmAdapter);
        progress = (ProgressBar) findViewById(R.id.progress);
        setupBookSelectedListener();
        // Get the films list
        fetchFilms();
    }

    public void setupBookSelectedListener() {
        lvFilms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Launch the detail view passing book as an extra
                Intent intent = new Intent(FilmListActivity.this, FilmDetailActivity.class);
                intent.putExtra(FILM_DETAIL_KEY, filmAdapter.getItem(position));
                startActivity(intent);
            }
        });
    }

    // Executes an API call to the given Films URL.
    private void fetchFilms() {
        // Show progress bar before making network request
        progress.setVisibility(ProgressBar.VISIBLE);
        client = new FilmClient();
        client.getFilms(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                // Hide progress bar
                progress.setVisibility(ProgressBar.GONE);
                JSONArray docs = null;
                if (response != null) {
                    // Parse json array into array of model objects
                    List<Film> films = Film.fromJson(response);
                    // Sort by year
                    Collections.sort(films, new Comparator<Film>() {
                        @Override
                        public int compare(Film o1, Film o2) {
                            return o2.getYear().compareTo(o1.getYear());
                        }
                    });
                    // Remove all books from the adapter
                    filmAdapter.clear();
                    // Load model objects into the adapter
                    for (Film film : films) {
                        filmAdapter.add(film);
                    }
                    filmAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.setVisibility(ProgressBar.GONE);
            }
        });
    }

}
