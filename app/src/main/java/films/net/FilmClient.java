package films.net;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

public class FilmClient {
    private static final String API_URL = "http://api.myjson.com/bins/3y6ku";
    private AsyncHttpClient client;

    public FilmClient() {
        client = new AsyncHttpClient();
    }

    // Method for accessing the search API
    public void getFilms(JsonHttpResponseHandler handler) {
        client.get(API_URL, handler);
    }

}
