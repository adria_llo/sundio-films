package films.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Film implements Serializable {
    private String title;
    private Integer year;
    private Double rating;
    private String sumary;
    private String poster;
    private String runtime;
    private String director;
    private String category;
    private List<String> cast;

    public Film(){
        cast = new ArrayList<>();
    }

    public void addCast(String actor){
        cast.add(actor);
    }

    // Returns a Book given the expected JSON
    public static Film fromJson(JSONObject jsonObject) {
        Film film = new Film();
        try {
            // Deserialize json into object fields
            film.title = jsonObject.getString("show_title");
            film.year = jsonObject.getInt("release_year");
            film.rating = jsonObject.getDouble("rating");
            film.sumary = jsonObject.getString("summary");
            film.poster = jsonObject.getString("poster");
            film.runtime = jsonObject.getString("runtime");
            film.director = jsonObject.getString("director");
            film.category = jsonObject.getString("category");
            for (String actor : jsonObject.getString("show_cast").split(", ")){
                film.addCast(actor);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return film;
    }

    // Decodes array of json results into business model objects
    public static List<Film> fromJson(JSONArray jsonArray) {
        List<Film> films = new ArrayList<>(jsonArray.length());
        // Process each result in json array, decode and convert to business object
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject filJson = null;
            try {
                filJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            Film film = Film.fromJson(filJson);
            if (film != null) {
                films.add(film);
            }
        }
        return films;
    }

    /**
     * Getters
     */

    public String getTitle() {
        return title;
    }

    public Integer getYear() {
        return year;
    }

    public Double getRating() {
        return rating;
    }

    public String getSumary() {
        return sumary;
    }

    public String getPoster() {
        return poster;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getDirector() {
        return director;
    }

    public String getCategory() {
        return category;
    }

    public List<String> getCast() {
        return cast;
    }

    public String getCastAsString() {
        String res = "";
        for (String actor : cast){
            if (!res.isEmpty()) res += ", ";
            res += actor;
        }
        return res;
    }
}

